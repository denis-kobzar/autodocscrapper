const dev = 'dev';
const prod = 'prod';

let devMode = dev;

const log = (msg) => {
	if (devMode === dev) {
		console.log(msg);
	}
}

module.exports = {
	log
}