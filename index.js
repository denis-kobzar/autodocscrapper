const cheerio = require('cheerio');
const axios = require('axios');
const fs = require('fs');
const csv = require('csv');

const rootUrl = 'https://autodoc.ua/category/';
const pagePattern = '?page=';

const categories = [
	{
		name: 'Масляный фильтр',
		url: 'maslyanyj-filtr-id170-3'
	},
	{
		name: 'Фильтр салона и компоненты',
		url: 'filtr-salona-i-komponenty-id273-3'
	}
];

const products = [];

const getHtml = async (url) => {
	return axios.get(url);
}

const getPages = ($) => {
	const href = $('a.art-pagination__second-control-link').attr('href');
	const regex = /page=(\d+)/;
	const match = href.match(regex);
	return match[1];
}

const saveIntoCsv = (fileName, data) => {
	csv.stringify(data, {
		header: true
	}, (err, output) => {
		fs.writeFile(__dirname + `/${fileName}`, output, (err) => {
			if (err) {
				console.log(err);
			}
		});	
	});
}

(async function app() {
	try {
		const outputData = [];
		for (category of categories) {
			console.log(`Category ${category.name} - downloading`);

			const categoryProducts = {
				category: category,
				products: []
			};

			const html = await getHtml(`${rootUrl}${category.url}`);
			let $ = cheerio.load(html.data);
			const pages = await getPages($);
			
			for (let page = 1; page <= pages; page++) {
				console.log(`${rootUrl}${category.url}?page=${page} - downloading`);
				const pageContent = await getHtml(`${rootUrl}${category.url}?page=${page}`);
				console.log(`${rootUrl}${category.url}?page=${page} - downloaded`);
				
				$ = cheerio.load(pageContent.data);
				
				$('.product-box').each((i, element) => {
					const payloadRegex = /Загрузить ещё/;
					if (!$(element).html().match(payloadRegex)) {
						const name = $('a.product-box__name', element).text();
						const url = 'https://autodoc.ua' + $('a.product-box__name', element).attr('href');
						const code = $('.product-box__code', element).text();
						const inStock = $('.product-box__instock', element).text() == 'Есть в наличии' ? true : false;
						const price = $('span.product-box__new-price', element).text();
						
						const codeRegex = /Бренд:(.+)Код.+:(.+)/;
						const brand = code.match(codeRegex)[1];
						const articul = code.match(codeRegex)[2];

						const product = {
							name: name,
							url: url,
							brand: brand,
							articul: articul,
							inStock: inStock,
							price: price
						};

						categoryProducts.products.push(product);
					}
				});
			};

			products.push(categoryProducts);
		}

		for (item of products) {
			console.log('Category: ', item.category.name);
			for (product of item.products) {
				product.inStock = product.inStock.toString();
				outputData.push(product);
			}
		}
		console.log('Saving...');
		saveIntoCsv('products.csv', outputData);
		console.log('Saved.');
	} catch (err) {
		console.log('Error: ' + err);
	}
})();