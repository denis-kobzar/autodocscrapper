const cheerio = require('cheerio');
const axios = require('axios');

const scrapper = require('./common.js');

const getHtml = async (url) => {
	try {
		const response = await axios.get(url);
		return response.data;
	} catch (e) {
		throw e;
	}
}

const getLinkItems = async (html, template) => {
	try {
		const links = [];
		let $ = cheerio.load(html);
		$(template).each((i, element) => {
			let href = 'https://autodoc.ua' + $(element).attr('href');
			links.push(href);
		});
		return links;
	} catch (e) {
		throw e;
	}
}

const getCategories = async (url) => {
	try {
		const subLinks = [];
		const mainHtml = await getHtml(url);
		const mainCategories = await getLinkItems(mainHtml, 'a.sub-categories__link');
		mainCategories.forEach(async link => {
			const subHtml = await getHtml(link);
			const subCategories = await getLinkItems(subHtml, 'a.sub-sub-link');
			subCategories.forEach(subLink => {
				subLinks.push(subLink);
			});
		});
		return subLinks;
	} catch (e) {
		throw e;
	}
}

(async function main() {
	const result = await getCategories('https://autodoc.ua/category');
	console.log(result.length);
} ())
